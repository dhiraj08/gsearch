Author - Dhiraj Nalawade
Bitbucket - https://bitbucket.org/dhiraj08/gsearch/src/master
Start-Script - npm start

Main file - app.js
    Contains - 
    1. Express app initialized.
    2. Set up bodyparser, view engine, db configuration, routes configuration, dotenv file and logger implementation.

dotenv file contains following configurations 
    1. jwtPrivateKey - key to be set which is consumed while signing jwt token.
    2. gApiKey - Google API key for fetching nearby places API data.
    3. PORT - Optional parameter to use some other port to listen.

Routes -
    1. POST http://3.17.164.65:4100/api/users
        req.body -
            email:dhiraj@gmail.com
            name:Dhiraj N
            password:asdasd
            gender:male

        Signup api. Gets token in response along with user object.
    
    2. POST http://3.17.164.65:4100/api/users/login
        req.body -
            email:dhiraj@gmail.com
            password:asdasd

        Login api to get authentication token in response along with user object.

    3. GET http://3.17.164.65:4100/api/users/search?search=restaurant&latitude=18.9894&longitude=73.1175

        req.query -
            search:restaurant
            latitude:18.9894
            longitude:73.1175

        headers -
            x-auth-token:eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZDEzM2JlZDg0M2YwODRlNTBmNmM0MGUiLCJpYXQiOjE1NjE1NDMyNTF9.3FDaSjKWVS407hzkfGV7vo8ag5I17S3z-wTlH5sLIok
            Content-Type: application/x-www-form-urlencoded

        i. Search places based on search keyword passed in the 'search' parameter. Also require users latitude and longitude so as to fetch nearby places. 
        ii. We first check if the search data exist in database or not. If not we fetch using google API and then store the response in database for future reference.
        iii. Search results are also catched in the system using the memory-cache.
        iv. User activity is recorded for all searched made by user.


Models -
    1. user - Contains user details and credentials to login.
    2. places - Contains data for items searched by user fetched from google nearby search api.
    3. activity - Contains records of keywords searched by users for analytics purpose.

Using the database we can extend the system to following -
    1. Do analytics on what major searches a user does.
    2. Track most frequently keywords searched.
    3. Track the loactions where most of the search are made by users based on the '2dsphere' location index we have in our database.

Access log is maintained in access.log file and a pm2 error log status can be found on server for errors.
Project hosted on server address - http://3.17.164.65:4100

