var express = require('express');
const auth = require('../middleware/auth');
var router = express.Router();
const bcrypt = require('bcrypt');
const {User} = require('../models/user');
const {Places} = require('../models/places');
const {Activity} = require('../models/activity');
const error = require('../middleware/error');
const cache = require('memory-cache');
const helper = require('../middleware/helper');
const cacheMiddleware = require('../middleware/cache');


/* POST user signup. */
router.post('/', async (req, res, next) => {
  
  let user = await User.findOne({ email: req.body.email });
  if(user) {
      return res.status(400).json({
        error: true,
        message: "User already registered"
    });
  }
  
  const {
    name,
    email,
    password,
    gender
  } = req.body;
  
  user = new User({
    name,
    email,
    password,
    gender
  });
  const salt = await bcrypt.genSalt(10);
  user.password = await bcrypt.hash(user.password, salt);
  console.log('salt', salt, ' user.password',  user)
  await user.save();

  const token = user.generateAuthToken();
  res.status(200).json({
    error: false,
    message: "User registered successfully",
    details: user,
    token: token
  });
});

/* POST Login api */
router.post('/login', async (req, res, next) => {
  try {
    let user = await User.findOne({ email: req.body.email });
    if (!bcrypt.compareSync(req.body.password, user.password)) {
      return res.status(400).json({
          error: true,
          title: 'Invalid password',
          detail: 'Invalid password'

      });
    }
    const token = user.generateAuthToken();
    res.status(200).json({
      error: false,
      message: "Login successful",
      details: user,
      token: token
    });
  }
  catch(err) {
    res.status(500).json({
      error: true,
      message: "Something wrong with the user credentials passed. Please check and try to login again.",
      details: err
    });
  }  
});

router.get('/search', auth, cacheMiddleware(), async (req, res, next) => {
  try{
    const activity = new Activity({
      keyword: req.query.search,
      user_id: req.user._id
    });
    activity.save(); // save user search activities for reference
    
    let place = await helper.getPlaces(req); // fetch from db based on search keyword and location
    
    let key =  '__gsearch__' + req.originalUrl || req.url
    if(place && place.length > 0) {
      // cache data
      cache.put(key, {
        error: false,
        message: "Data fetched successfully.",
        details: place
      }, 20000, function(key, value) {
        console.log(key + ' ---did--- ' + value);
      });

      return res.status(200).json({
        error: false,
        message: "Data fetched successfully.",
        details: place
      });
    }
    let gSeachData = await helper.getSearchResultFromAPI(req);

    cache.put(key, {
      error: false,
      message: "Data fetched successfully.",
      details: gSeachData
    }, 20000, function(key, value) {
      console.log(key + ' ---did--- ' + value);
    });

    res.status(200).json({
      error: false,
      message: "Data fetched successfully.",
      details: gSeachData
    });
    const places = new Places({
      keyword: req.query.search,
      location: {type: 'Point', coordinates:[req.query.latitude, req.query.longitude]},  
      details: gSeachData
    });

    // save places in database for future reference
    places.save();
  }
  catch (err) {
    res.status(500).json({
      error: true,
      message: "Something wrong wrong. Please try again later.",
      details: err
    });
  }
});

module.exports = router;
