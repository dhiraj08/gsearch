const bcrypt = require('bcrypt');
const _ = require('lodash');
const {User} = require('../models/user');
const express = require('express');
const router = express.Router();

router.post('/', async (req,res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    
    let user = await User.findOne({ email: req.body.email });
    if(!user) return res.status(400).send('Email or password invalid');

    const validpassword = await bcrypt.compare(req.body.password, user.password);
    if(!validpassword) return res.status(400).send('Email or password invalid');

    const token = user.generateAuthToken();
    res.send(token);
});

function validate(req) {
    const sechma = {
        email: Joi.string().min(5).max(255).email().required(),
        password: Joi.string().min(5).max(255).required()
    }

    return Joi.validate(req, sechma);
}

module.exports = router;