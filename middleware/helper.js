const Request = require('request');
const {Places} = require('../models/places');

const getSearchResultFromAPI = (req) => {
    return new Promise((resolve, reject) => {
        Request.get(`https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${req.body.latitude},${req.body.longitude}&radius=1500&type=restaurant&keyword=${req.body.search}&key=${process.env.gApiKey}`, (error, response, body) => {
            if(error) {
                return reject(error);
            } else {
                return resolve(JSON.parse(body));
            }
        });
    });
};

// [18.98, 73.11]
const getPlaces = (req) => {
    return new Promise((resolve, reject) => {
        Places.find({
            keyword: {$regex: req.query.search},
            location: { $near : 
                { 
                    $geometry :
                    {
                        type : "Point" ,
                        coordinates : [req.query.latitude, req.query.longitude] // [18.98, 73.11]
                    },
                    $maxDistance : 50000
                }
            }
        }, {"details.results": 1, _id: 0})
        .exec((err, data) => {
            if(err)
                return reject(err);
            else
                return resolve(data);
          });
    });
}

module.exports = {
    getSearchResultFromAPI,
    getPlaces
}