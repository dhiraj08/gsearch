module.exports = function(err, req, res, next){
    res.status(200).json({
        error: true,
        message: "Something went wrong",
        details: err
    });
}