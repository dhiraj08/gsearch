var cache = require('memory-cache');
let cacheMiddleware = () => {
    return (req, res, next) => {
        let key =  '__gsearch__' + req.originalUrl || req.url
        let cacheContent = cache.get(key);
        if(cacheContent){
            return res.status(200).json( cacheContent );
        }else{
            next()
        }
    }
}

module.exports = cacheMiddleware;