var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var morgan = require('morgan');
var fs = require('fs');
var bodyParser = require('body-parser');
var dotenv=require('dotenv').config();

/* var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users'); */

var app = express();

// req body limit setup
app.use(bodyParser.json({limit: '100mb', extended: true}));
app.use(bodyParser.urlencoded({limit: '20mb', extended: true}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');



// create a write stream (in append mode)
var accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' })
 
// setup the logger
app.use(morgan('combined', { stream: accessLogStream }));
app.use(morgan('dev'));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// database setup
require('./config/db')();

// app.use('/', indexRouter);
// app.use('/users', usersRouter);

require('./config/routes')(app);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

process.on('uncaughtException', (ex) => {
  console.log('We Got an Uncaught Exception', ex);
  throw ex;
});

process.on('unhandledRejection', (ex)=> {
  console.log('We Got an Unhandled rejection', ex);
  throw ex;
});

module.exports = app;
