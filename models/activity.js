const mongoose = require('mongoose');

const activitySchema = new mongoose.Schema({
    keyword: {
        type: String,
        required: true
    },
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user'
    }
},
{
  timestamps: true
});

const Activity = mongoose.model('Activity', activitySchema);

exports.Activity = Activity;