const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        minlenght: 5,
        maxlength: 50
    },
    email: {
        type: String,
        required: true,
        unique: true,
        minlenght: 5,
        maxlength: 255
    },
    password: {
        type: String,
        required: true,
        minlenght: 5,
        maxlength: 1024
    },
    gender: {
      type: String,
      enum: ['male', 'female', 'other']
    }
},
{
  timestamps: true
});

userSchema.methods.generateAuthToken = function () {
    console.log(' process.env.jwtPrivateKey',  process.env.jwtPrivateKey)
    return jwt.sign({ _id: this._id }, process.env.jwtPrivateKey);
}

const User = mongoose.model('User', userSchema);

exports.User = User;