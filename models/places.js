const mongoose = require('mongoose');

const placesSchema = new mongoose.Schema({
    keyword: {
        type: String,
        required: true
    },
    location: {
        type: {
            type: String,
            enum: ['Point'],
            required: true
        },
        coordinates: {
            type: [Number],
            require: true
        }
    },
    details: {
        type: mongoose.Schema.Types.Mixed,
        required: false
    }
},
{
  timestamps: true
});

// Create GeoJSON Point index
placesSchema.index({location: '2dsphere'});

const Places = mongoose.model('Places', placesSchema);

exports.Places = Places;